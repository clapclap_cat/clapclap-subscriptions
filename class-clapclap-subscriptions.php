<?php
/**
 * Plugin Name: ClapClap Subscriptions
 */
class ClapClap_Subscriptions {
	public static function get_user_current_subscription( int $user_id = null ): WC_Subscription|null {
		if ( null === $user_id ) {
			$user_id = get_current_user_id();
		}
		$all_subscriptions = wcs_get_users_subscriptions( $user_id );
		foreach ( $all_subscriptions as $subscription ) {
			if ( ! self::subscription_is_active_or_pending( $subscription ) ) {
				continue;
			}
			$start_date   = DateTime::createFromFormat( 'Y-m-d H:i:s', $subscription->get_date( 'start' ) );
			$next_date    = DateTime::createFromFormat( 'Y-m-d H:i:s', $subscription->get_date( 'next_payment' ) );
			$current_date = new DateTime();
			if ( $current_date > $start_date && $current_date < $next_date ) {
				return $subscription;
			}
		}
		return null;
	}
	public static function user_is_subscribed( int $user_id = null ): bool {
		if ( null === $user_id ) {
			$user_id = get_current_user_id();
		}
		return self::get_user_current_subscription( $user_id ) !== null;
	}
	public static function current_user_is_subscribed_or_admin(): bool {
		return self::user_is_subscribed() || current_user_can( 'administrator' );
	}
	public static function get_subscription_monthly_contribution( WC_Subscription $subscription ): float {
		if ( ! $subscription ) {
			return 0;
		}
		if ( 'year' === $subscription->billing_period ) {
			return $subscription->total / 12;
		}
		return $subscription->total;
	}
	public static function subscription_is_active_or_pending( WC_Subscription $subscription ) : bool {
		return 'active' === $subscription->status || 'pending' === $subscription->status;
	}
}
